



def gen_estructura(names,goals,goals_avoided,assists):
    """
        La funcion recibe por parametro las listas correspondientes (estas se encuentran ordenadas por posicion)
        , con ellas primero toma la lista que contiene los nombres de los jugadores y los separa en una lista
        para mayor facilidad al momento de juntar todas. Luego utilizo la funcion zip que empaqueta las listas en un archivo
        zip, dentro se encuentras los jugadores asociados a sus respectivas estadisticas. Por ultimo el retorno
        devuelve una lista de tuplas que cada tupla contiene la informacion ordenada de los jugadores.
    """

    list_names = names.replace(",","").split()
    zip_players = zip(list_names,goals,goals_avoided,assists)
    return list(zip_players)


def search_scorer(list_players):
    """
        Recibe la lista total de jugadores, crea un diccionario para asignarle el jugador goleador 
        y procede a buscarlo dentro de la lista anteriormente dicha. Se puede ver que utilizo como variable comparativa
        el key "goals" dentro del diccionario. Por ultimo retorno el diccionario de ese jugador estrella.
    """
    
    player_scorer = {
        'name': "",
        'goals': -1
    }
    for player in list_players:
        if (player[1]>player_scorer['goals']):
            player_scorer['name']= player[0]
            player_scorer['goals']= player[1]
    return (player_scorer)


def search_influential(list_players):
    """
        La funcion recibe la lista de jugadores completa en busca de encontrar el jugador con mayor influencia en el equipo,
        primero creo el diccionario con el posteriormente sera devuelto el jugador previamente dicho, luego recorro
        la lista generando en cada jugador su promedio ponderado y consultado a la key "average" del diccionario creado si es
        mayor que el guardado, en caso de que lo sea actualiza el nombre y el promedio del diccionario. Por ultimo
        retorna un diccionario con keys "name" y "average" que tendran como value la informacion del jugador
        mas influyente del equipo.
    """

    influential= {
        'name' : "",
        'average' : -1
    }
    
    for player in list_players:
        average = (player[1] * 1.5 + player[2] * 1.25 + player[3])/3
        if influential['average']< average:
            influential['name'] = player[0]
            influential['average'] = average
    return influential